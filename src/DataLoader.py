#data loader
import os
import random
import numpy as np
import cv2
from Preprocessor import preprocess

class Sample:
    #Sample
    def __init__(self, gtText, filePath):
        self.gtText = gtText
        self.filePath = filePath     #path the to .png    ../data/words/


#Batch
class Batch: 
    # batch contains list of images and ground truth texts
    print("Batch created")
    def __init__ (self, gtTexts, imgs):
        self.imgs = np.stack(imgs, axis=0)
        self.gtTexts = gtTexts
class DataLoader:
    #load data with IAM format


    def __init__(self, filePath, batchSize, imgSize, maxTextLen):
        print("data loader (", filePath, ", ", batchSize, ", ", imgSize, ", ", maxTextLen, ")")
        
        self.i = 1  # just for test
        
        self.dataAug = False
        self.currIndex = 0
        self.batchSize = batchSize
        self.imgSize = imgSize
        self.samples = []

        words = open(filePath + "words.txt")
        chars = set()
        bad_samples = []
        bad_samples_reference = ['a01-117-05-02.png', 'r06-022-03-05.png']        

        index = 0    #just for test
        
        for line in words:
            #ignore comment line
            if not line or line[0]=='#':
                continue

            lineSplit = line.strip().split(' ')
            assert len(lineSplit) >= 9
            
            # get the correspoding png name in data/words
            fileNameSplit = lineSplit[0].split('-')
            fileName = filePath + 'words/' + fileNameSplit[0] + '/' + fileNameSplit[0] + '-' + fileNameSplit[1] + '/' + lineSplit[0] + '.png'
            
            # get the ground ture text starting from index at 8
            gtText = self.truncateLable(' '.join(lineSplit[8:]), maxTextLen)
            # record chars appered in the words
            chars = chars.union(set(list(gtText)))

            if not os.path.getsize(fileName):
                bad_samples.append(lineSplit[0] + ".png")
                continue
            
            # put good sample into tje sample list
            self.samples.append(Sample(gtText, fileName))

            
            index += 1
            if index <=0:
                print(lineSplit)
                print(fileNameSplit)
                print(fileName)
                print(gtText)
                print(chars)
                print(bad_samples)
                print("-------------------------------")       
        # after read all the lines in word.txt

        # handle damaged image
        if set(bad_samples) != set(bad_samples_reference):
            print("Warning, found a damaged image", bad_samples)
            print("Expected damage images: ", bad_samples_reference)

        # split Samples into training and validation set: 95% - 5%
        splitIndex = int(0.95 * len(self.samples))
        self.trainSamples = self.samples[:splitIndex]
        self.validationSamples = self.samples[splitIndex:]
        # print(len(self.trainSamples))
        # print(len(self.validationSamples))

        # put ground true words in to lists
        self.trainWords = [x.gtText for x in self.trainSamples]
        self.validationWords = [x.gtText for x in self.validationSamples]
        # print(len(self.trainWords))
        # print(len(self.validationWords))

        # number of randomly chosen samples per epoch for training
        self.numTSperEpoch = 25000

        # start train set
        self.trainSet()

        # list of all chars appeared in the dataset
        self.charList = sorted(list(chars))
        #print(self.charList)



    def truncateLable(self, text, maxTextLen):
        """ctc_loss can't compute loss if it cannot find a mapping between text label and input 
		   labels. Repeat letters cost double because of the blank symbol needing to be inserted.
		   If a too-long label is provided, ctc_loss returns an infinite gradient """
        # handle a too-long label
        cost = 0
        for i in range(len(text)):
            if i != 0 and text[i] == text[i-1]:
                cost += 2
            else:
                cost += 1
            if cost > maxTextLen:
                return text[:i]
        return text

    
    def trainSet(self):
        """ switch samples to random shuffle trainSamples """
        self.dataAug = True
        self.currIndex = 0
        random.shuffle(self.trainSamples)
        self.samples = self.trainSamples[:self.numTSperEpoch]
        print("samples -> trainSample")


    def validationSet(self):
        """ switch to validation samples """
        self.dataAug = False
        self.currIndex = 0
        self.samples = self.validationSamples
        
    def getIteratorInfo(self):
        return (self.currIndex // self.batchSize + 1, len(self.samples) // self.batchSize)
    
    # has next batch
    def hasNext(self):
        #self.i += 1
        return self.currIndex + self.batchSize <= len(self.samples)
    
    # get next batch
    def getNext(self):
        #print("get next batch")
        bRange = range(self.currIndex, self.currIndex + self.batchSize)
        gtTexts = [self.samples[i].gtText for i in bRange]
        imgs = [preprocess(cv2.imread(self.samples[i].filePath, cv2.IMREAD_GRAYSCALE), self.imgSize, self.dataAug) for i in bRange]
        self.currIndex += self.batchSize
        return Batch(gtTexts, imgs)
