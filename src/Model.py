#model
#from __future__ import division
#from __future__ import print_function

import sys
import numpy as np
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

#DecoderType
class DecoderType:
    BestPath = 0
    BeamSearch = 1
    WordBeamSearch = 2


class Model:
    #TensorFlow model for HTR

    #constants
    batchSize = 50
    imgSize = (128,32)
    maxTextLen = 32

    def __init__(self, charList, decoderType = DecoderType.BestPath, mustRestore=False, dump = False):
        print("Create Model")
        self.dump = dump
        self.charList = charList
        self.decoderType = decoderType
        self.mustRestore = mustRestore
        self.snapID = 0   # keep track of saved model
        
        # Whether to use normalization over a batch ot a population
        self.is_norm = tf.compat.v1.placeholder(tf.bool, name='is_norm')
        
        # input image batch
        self.inputImg = tf.compat.v1.placeholder(tf.float32, shape=(None, Model.imgSize[0], Model.imgSize[1]))
        print("tf: ", self.inputImg)
        
        # set up CNN, RNN, and CTC
        self.setupCNN()
        self.setupRNN()
        self.setupCTC()


        # set up optimizer to train Neural Network
        self.batchesTrained = 0
        self.learningRate = tf.compat.v1.placeholder(tf.float32, shape=[])
        self.update_ops = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(self.update_ops):
            self.optimizer = tf.compat.v1.train.RMSPropOptimizer(self.learningRate).minimize(self.loss)

        #init TF
        (self.session, self.saver) = self.setupTF()


    def setupCNN(self):
        print("-------------------------------------------create CNN layers -----------------------------------------")
        cnnIn4d = tf.expand_dims(input=self.inputImg, axis=3)
        print("cnnIn4d: ", cnnIn4d)

        # parameters for the layers
        kernelVals = [5,5,3,3,3]
        featureVals = [1,32,64,128,128,256]
        strideVals = poolVals = [(2,2), (2,2), (1,2), (1,2), (1,2)]
        numLayers = len(strideVals)

        # create layers
        pool = cnnIn4d   # input
        for i in range (numLayers):
            kernel = tf.Variable(tf.random.truncated_normal([kernelVals[i], kernelVals[i], featureVals[i], featureVals[i+1]], stddev=0.1))   # weight
            conv = tf.nn.conv2d(input=pool,filters=kernel, padding='SAME', strides=(1,1,1,1))  # input, weight, bias, strides
            conv_norm = tf.compat.v1.layers.batch_normalization(conv, training=self.is_norm)
            relu = tf.nn.relu(conv_norm)
            pool = tf.nn.max_pool2d(input=relu, ksize=(1, poolVals[i][0], poolVals[i][1], 1), strides=(1, strideVals[i][0], strideVals[i][1], 1), padding='VALID')
            print("layer: ", i)
            print("kernel: ", kernel)
            print("conv: ", conv)
            print("conv_norm: ", conv_norm)
            print("relu: ", relu)
            print("pool: ", pool)
        
        self.cnnOut4d = pool
           
        

    def setupRNN(self):
        print("--------------------------------------------create RNN layers -------------------------------------------")
        rnnIn3d = tf.squeeze(self.cnnOut4d, axis=[2])

        # basic cells uesed to build RNN
        numHidden = 256
        cells = [tf.compat.v1.nn.rnn_cell.LSTMCell(num_units=numHidden, state_is_tuple=True) for _ in range(2)]   # 2 layers

        # stack basic cells
        stacked = tf.compat.v1.nn.rnn_cell.MultiRNNCell(cells, state_is_tuple=True)

        # bidirectional RNN
        # BxTxf -> BxTx2H
        ((fw, bw), _) = tf.compat.v1.nn.bidirectional_dynamic_rnn(cell_fw=stacked, cell_bw=stacked, inputs=rnnIn3d, dtype=rnnIn3d.dtype)

        # BxTxH + BxTxH -> BxTx2H -> BxTx1x2H
        concat = tf.expand_dims(tf.concat([fw, bw], 2), 2)

        # project output to chars (including blank): BxTx1x2H -> BxTx1xC -> BxTxC
        kernel = tf.Variable(tf.random.truncated_normal([1, 1, numHidden * 2, len(self.charList) + 1], stddev=0.1))
        self.rnnOut3d = tf.squeeze(tf.nn.atrous_conv2d(value=concat, filters=kernel, rate=1, padding='SAME'), axis=[2])

        print("rnnIn3d: ", rnnIn3d)
        print("cells: ", cells)
        print("stacked: ", stacked)
        print("((fw, bw), _)", ((fw, bw), _))
        print("concat: ", concat)
        print("kernel: ", kernel)
        print("rnnOut3d: ", self.rnnOut3d)


    def setupCTC(self):
        print("----------------------------------------------create CTC layers ------------------------------------------------")
        # BxTxC -> TxBxC
        self.ctcIn3dTBC = tf.transpose(a=self.rnnOut3d, perm=[1,0,2])
        # ground truth text as sparse tensor
        self.gtTexts = tf.SparseTensor(tf.compat.v1.placeholder(tf.int64, shape=[None,2]), tf.compat.v1.placeholder(tf.int32, [None]), tf.compat.v1.placeholder(tf.int64, [2]))

        # calculate loss for batch
        self.sequenceLen = tf.compat.v1.placeholder(tf.int32, [None])
        self.loss = tf.reduce_mean(input_tensor=tf.compat.v1.nn.ctc_loss(labels=self.gtTexts, inputs=self.ctcIn3dTBC, sequence_length=self.sequenceLen, ctc_merge_repeated=True))

        # calculate loss for each element to compute label probability
        self.savedCtcInput = tf.compat.v1.placeholder(tf.float32, shape=[Model.maxTextLen, None, len(self.charList) + 1])
        self.lossPerElement = tf.compat.v1.nn.ctc_loss(labels=self.gtTexts, inputs=self.savedCtcInput, sequence_length=self.sequenceLen, ctc_merge_repeated=True)
        # decoder
        if self.decoderType == DecoderType.BestPath:
            self.decoder = tf.nn.ctc_greedy_decoder(inputs=self.ctcIn3dTBC, sequence_length=self.sequenceLen)
        else:
            self.decoder = "no decoder"

        print("ctcIn3d: ", self.ctcIn3dTBC)
        print("gtTexts: ", self.gtTexts)
        print("sequenceLen: ", self.sequenceLen)
        print("loss: ", self.loss)
        print("savedCtcInput: ", self.savedCtcInput)
        print("lossPerElement: ", self.lossPerElement)
        print("decoder: ", self.decoder)

    def setupTF(self):
        print("------------------------------------------------setup Tensorflow-------------------------------------------------")
        print("Python: ", sys.version)
        print("Tensorflow: ", tf.__version__)

        # TF session
        sess = tf.compat.v1.Session()
        # save model to file
        saver = tf.compat.v1.train.Saver(max_to_keep=1)
        modelDir = "../model/"
        # to check is there a saved model
        latestSnapshot = tf.train.latest_checkpoint(modelDir)

        if (self.mustRestore and not latestSnapshot):
            raise Exception("cannot find saved model in: ", modelDir)

        # load saved model if avaliable
        if latestSnapshot:
            print("init with saved values from ", latestSnapshot)
            saver.restore(sess, latestSnapshot)
        else:
            print("init with new values")
            sess.run(tf.compat.v1.global_variables_initializer())

        return (sess, saver)

    def trainBatch(self, batch):
        #print("train batch")
        numBatchElement = len(batch.imgs)
        sparse = self.toSparse(batch.gtTexts)
        rate = 0.01 if self.batchesTrained < 10 else (0.001 if self.batchesTrained < 10000 else 0.0001) # learning rate decay
        evalList = [self.optimizer, self.loss]
        feedDict = {self.inputImg: batch.imgs, self.gtTexts: sparse, self.sequenceLen: [Model.maxTextLen]*numBatchElement, self.learningRate: rate, self.is_norm: True}
        (_, lossVal) = self.session.run(evalList, feedDict)
        self.batchesTrained +=1
        return lossVal

    def toSparse(self, gtTexts):
        # put ground truth texts into sparse tensor for ctc_loss
        #print("test toSparse")
        indices = []
        values = []
        shape = [len(gtTexts), 0]

        # travelsal all input gtTexts
        for (element, text) in enumerate(gtTexts):
            # convert to string of label
            label = [self.charList.index(i) for i in text]
            # sparse tensor has size of max label
            if len(label) > shape[1]:
                shape[1] = len(label)
            # put each label in to sparse tensor
            for (i, l) in enumerate(label):
                indices.append([element, i])
                values.append(l)

        return (indices, values, shape)

    def save(self):
        self.snapID += 1
        self.saver.save(self.session, "../model/snapshot", global_step=self.snapID)

    def inferBatch(self, batch, calcProbability=False, probabilityOfGT=False):
        # decode, optionally save RNN output
        numBatchElements = len(batch.imgs)
        evalRnnOutput = self.dump or calcProbability
        evalList = [self.decoder] + ([self.ctcIn3dTBC] if evalRnnOutput else [])
        feedDict = {self.inputImg: batch.imgs, self.sequenceLen: [Model.maxTextLen]*numBatchElements, self.is_norm: False}
        evalRes = self.session.run(evalList, feedDict)
        decoded = evalRes[0]
        texts = self.decoderOutputToText(decoded, numBatchElements)

        # feed RNN output and recognized text onto CTC loss to compute labeling probability
        probs = None
        if calcProbability:
            sparse = self.toSparse(batch.gtTexts) if probabilityOfGT else self.toSparse(texts)
            ctcInput = evalRes[1]
            evalList = self.lossPerElement
            feedDict = {self.savedCtcInput: ctcInput, self.gtTexts: sparse, self.sequenceLen: [Model.maxTextLen]*numBatchElements, self.is_norm: False}
            lossVals = self.session.run(evalList, feedDict)
            probs = np.exp(-lossVals)

        return (texts, probs)

    def decoderOutputToText(self, ctcOutput, batchSize):
        # extract texts from output of CTC decoder

        # contain string of label for each batch element
        encodedLabelStrs = [[] for i in range(batchSize)]

        # ctc returns tuple, first element is SparseTensor
        decoded = ctcOutput[0][0]

        # traversal all indices and save mapping: batch -> values
        idxDict = {b : [] for b in range(batchSize)}
        for(idx, idx2d) in enumerate (decoded.indices):
            label = decoded.values[idx]
            batchElement = idx2d[0]
            encodedLabelStrs[batchElement].append(label)

        return [str().join([self.charList[c] for c in labelStr]) for labelStr in encodedLabelStrs]
