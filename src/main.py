import sys
import argparse
import cv2
import editdistance
from DataLoader import DataLoader
from Model import Model, DecoderType
from Preprocessor import preprocess

class FilePath:
    # paths to all the files needed
    fpTrain = '../data/'
    fpCharList = '../model/charList.txt'
    fpCorpus = '../data/corpus.txt'
    fpAccuracy = '../model/accuracy.txt'

def train(model, loader):
    print("---------------------------------------training start----------------------------------------------")
    epoch = 0
    noImprovement = 0 # keep track the num of epochs having no improvement on error rate
    bestCharErrorRate = float('inf') # best validation error rate
    stopping = 5 # stop when the num of epoch without improvement reach 5
    
    flag = True   # just for test
    while True:
        epoch += 1
        print("Epoch: ", epoch)

        # train
        print("train Neural Network")
        loader.trainSet()   # switch to trainSet
        while loader.hasNext():
            iterInfo = loader.getIteratorInfo()
            batch = loader.getNext()
            loss = model.trainBatch(batch)
            print("Batch:", iterInfo[0], '/', iterInfo[1], "Loss: ", loss)



        #validate
        charErrorRate = validate(model, loader)

        # save model if best validation accuracy
        if charErrorRate < bestCharErrorRate:
            print("Error rate improved, save model")
            bestCharErrorRate = charErrorRate
            noImprovement = 0
            model.save()
            open(FilePath.fpAccuracy, 'w').write("Validation char error rate of saved model: %f%%" % (charErrorRate * 100.0))
        else:
            print("Error rate not improved")
            noImprovement += 1
        
        # stop train
        if (noImprovement >= stopping):
            print("no improvement made for %d epoches. Stop Training" % stopping)
            break
        
        # just fo test
        if epoch == 5:
            flag = False


def validate(model, loader):
    print("validate Neural Network")
    loader.validationSet()
    numCharError = 0
    numCharTotal = 0
    numWordC = 0
    numWordTotal = 0

    while loader.hasNext():
        iterInfo = loader.getIteratorInfo()
        print("Batch:", iterInfo[0], "/", iterInfo[1])
        batch = loader.getNext()
        (recognized, _) = model.inferBatch(batch)

        print("Ground truth -> Recognized")
        for i in range(len(recognized)):
            numWordC += 1 if batch.gtTexts[i] == recognized[i] else 0
            numWordTotal += 1
            dist = editdistance.eval(recognized[i], batch.gtTexts[i])
            numCharError += dist
            numCharTotal += len(batch.gtTexts[i])
            print("[Correct]" if dist == 0 else "[ERR:%d]" % dist, "(" + batch.gtTexts[i] + ")", "->", "(" + recognized[i] + ")")
    # print result
    charErrorRate = numCharError / numCharTotal
    wordAccuracy = numWordC / numWordTotal
    print("Char error rate: %f%%; Word accuracy: %f%%" % (charErrorRate * 100.0, wordAccuracy * 100.0))

    return charErrorRate


def main():
    print("hello world")
    #handle command line args
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', help="train model", action='store_true')
    parser.add_argument('--validate',help='check validation', action='store_true')

    args = parser.parse_args()
    
    # decoderType
    decoderType = DecoderType.BestPath
    # other decoder


    if args.train or args.validate:
        print("args = train or validate")
        # load data to train and create TF model
        loader = DataLoader(FilePath.fpTrain, Model.batchSize, Model.imgSize, Model.maxTextLen)

        # save chars appeared in dataSet to ../model/charList.txt
        open(FilePath.fpCharList, 'w').write(str().join(loader.charList))

        # save words contained in dataset into file   ../data/corpus.txt 
        open(FilePath.fpCorpus, 'w').write(str(' ').join(loader.trainWords + loader.validationWords))

        # training or validation
        if args.train:
            model = Model(loader.charList, decoderType)
            train(model, loader)
        elif args.validate:
            model = Model(loader.charList, decoderType, mustRestore=True)
            validate(model, loader)


if __name__ == "__main__" :
    main()
